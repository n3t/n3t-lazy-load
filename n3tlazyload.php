<?php
/**
 * @package n3t Lazy Load
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

class plgSystemN3tLazyLoad extends JPlugin {

	public function onBeforeCompileHead()
	{
	}

  public function onAfterRender()
  {
    $html = JFactory::getApplication()->getBody(false);

		preg_replace_callback('~(<img\s[^>]>)~i', array($this, 'parseImageTag'), $html);

    JFactory::getApplication()->setBody($html);
  }

	private function parseImageTag($matches)
	{
    $img = new SimpleXMLElement($matches[0]);
		return $matches[0];
	}

}
